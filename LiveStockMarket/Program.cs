using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;

namespace LiveStockMarket
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args)
        {
            return WebHost.CreateDefaultBuilder(args)
            //.UseKestrel(options =>
            //{            
            //    options.Listen(IPAddress.Loopback, 443, listenOptions =>
            //    {
            //        var serverCertificate = LoadCertificate();
            //        listenOptions.UseHttps(serverCertificate); // <- Configures SSL
            //    });
            //}).UseStartup<Startup>().Build();


            .UseKestrel(options =>
            {
                options.Listen(IPAddress.Any, 443, listenOptions =>
                {
                    listenOptions.UseHttps("cert.pfx", "1234");
                });
            }).UseStartup<Startup>().Build();



        }



        private static X509Certificate2 LoadCertificate()
        {
            var assembly = typeof(Startup).GetTypeInfo().Assembly;
            var embeddedFileProvider = new EmbeddedFileProvider(assembly, "LiveStockMarket");
            var certificateFileInfo = embeddedFileProvider.GetFileInfo("livestockmarketkey.pfx");
            using (var certificateStream = certificateFileInfo.CreateReadStream())
            {
                byte[] certificatePayload;
                using (var memoryStream = new MemoryStream())
                {
                    certificateStream.CopyTo(memoryStream);
                    certificatePayload = memoryStream.ToArray();
                }

                return new X509Certificate2(certificatePayload, "210294");
            }
        }
    }
}
