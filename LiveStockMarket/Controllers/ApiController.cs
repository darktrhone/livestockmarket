using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using LiveStockMarket.Infrastructure.External.Models;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Globalization;

namespace LiveStockMarket.Controllers
{
    public class ApiController : Controller
    {
        private readonly IHubContext<Hub> Hub;
        public static List<Stock> AllStocks = new List<Stock>();
        public static List<Stock> CryptoStocks = new List<Stock>();

        public ApiController(IHubContext<Hub> hub)
        {
            Hub = hub;
        }

        [HttpPost]
        public void EmitAllStocks([FromBody]Stock[] stocks)
        {
            AllStocks.AddRange(stocks);

            cleanStocks(AllStocks);

            Hub.Clients.All.SendAsync("AllStocks", stocks);
        }
        [HttpPost]
        public void EmitCryptoCurrency([FromBody]Stock[] stocks)
        {
            CryptoStocks.AddRange(stocks);

            cleanStocks(CryptoStocks);
   
            Hub.Clients.All.SendAsync("CryptoCurrency", CryptoStocks);
        }

        [HttpGet]
        public JsonResult GetStockByID(string ID)
        {
            if (AllStocks == null || AllStocks.Count() == 0 || string.IsNullOrEmpty(ID))
                return new JsonResult(string.Empty);

            var res = AllStocks.Where(x => x.title == ID);

            return new JsonResult(res);
        }

        private void cleanStocks(List<Stock> stocks)
        {
            var dt = DateTime.Now - TimeSpan.FromSeconds(15);

            stocks.RemoveAll(x => DateTime.ParseExact(x.lastUpdated, "HH:mm:ss", CultureInfo.InvariantCulture) < dt);

        }
    }
}