 
using System.Diagnostics;
using System.Threading.Tasks;
using LiveStockMarket.Infrastructure.Domain.Services;
using Microsoft.AspNetCore.Mvc;

namespace LiveStockMarket.Controllers
{
    public class BaseController : Controller
    {



        public virtual IActionResult Index()
        {
           
            return View();


            
        }

        public IActionResult Error()
        {
            ViewData["RequestId"] = Activity.Current?.Id ?? HttpContext.TraceIdentifier;
            return View();
        }
    }
}
