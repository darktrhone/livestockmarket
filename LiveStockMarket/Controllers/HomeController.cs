using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using LiveStockMarket.Infrastructure.Domain.Services;
using LiveStockMarket.Infrastructure.External.Models;
using Microsoft.AspNetCore.Mvc;

namespace LiveStockMarket.Controllers
{
    public class HomeController : BaseController
    {
        public override IActionResult Index()
        {
            return View();
        }

    }
}
