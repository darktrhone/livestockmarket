﻿import { Injectable } from '@angular/core'

import { StockEntity } from '../shared/models/stockentity.model'
import { Http, RequestOptions } from '@angular/http'
import { Observable } from "rxjs"
import 'rxjs/add/operator/map'
 


@Injectable()
export class StockService {

    constructor(private http: Http) { }
 
    public getStockByID(ID: string): Observable<StockEntity[]> {
        return this.http.get("/Api/GetStockByID", new RequestOptions({ params: { ID: ID } })).map(res => { 
            return res.json().map((item: StockEntity) => { 
                return item;
            });
        })
    }
}