﻿import { Component, Injectable, EventEmitter, Output } from '@angular/core'
import { HubConnection } from '@aspnet/signalr'
import { StockEntity } from '../shared/models/stockentity.model'
import { Subject } from 'rxjs/Subject'
import { Observable } from 'rxjs'


@Injectable()
export class StockHubService {

    private hubConnection: HubConnection = new HubConnection("/StockHub");


    constructor() { this.startConnection();  }

    ngOnInit() {    }


    connectMethod(method: string, emitter: EventEmitter<StockEntity[]>) { 
        this.hubConnection.on(method, obj => { 
            emitter.emit(obj);
        });
         
    }

    disconnectMethod(method: string) { 
        this.hubConnection.off(method); 
    }

    startConnection() {
         
        this.hubConnection
            .start()
            .then(() => console.log('Connection started!'))
            .catch(err => console.log(`Error -- ${err}`));
    }
      
}