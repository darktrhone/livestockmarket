import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule } from '@angular/forms'
import { RouterModule } from '@angular/router'
import { Routes } from '@angular/router'
import { AppComponent } from './components/app/app.component'
import { NavMenuComponent } from './components/navmenu/navmenu.component'
import { HomeComponent } from './components/home/home.component'
import { SearchComponent } from './components/search/search.component'
import { RouteReuseStrategy } from '@angular/router'
import { CustomReuseStrategy } from './shared/routing/routing'

export const routes: Routes = [
    { path: '', component: HomeComponent, pathMatch: 'full' },
    { path: 'stocks', loadChildren: './stock.module#StockModule' }
]


@NgModule({
    declarations: [
        AppComponent,
        NavMenuComponent,
        HomeComponent,
        SearchComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forRoot(routes)
    ],
    providers: [
        { provide: RouteReuseStrategy, useClass: CustomReuseStrategy },
    ],
    bootstrap: [AppComponent]
})
export class AppModuleShared {
}
