import { Component, Input } from '@angular/core'
import { StockEntity } from '../../models/stockentity.model'
import { Chart, TickOptions } from 'chart.js'
import '../../../../../wwwroot/assets/js/chartjs-plugin-datalabels'
import { AfterViewInit, OnInit } from '@angular/core/src/metadata/lifecycle_hooks'
import { StockService } from '../../../services/stock.service'
import { forEach } from '@angular/router/src/utils/collection'


@Component({
    selector: 'stockchart',
    templateUrl: './stockchart.component.html',
    styleUrls: ['./stockchart.component.css']

})
export class StockChartComponent implements AfterViewInit, OnInit {

    @Input() public initialStocksValue: StockEntity[] = [];

    @Input() public stocksValue: StockEntity[] = [];

    @Input() public chartName: string = "";

    public chart: Chart = new Chart("", {});

    public _arrLabel: string[] = [];

    public _arr: number[] = [];


    private max: number = 0;
    private min: number = 0;
    private diff: number = 0;
    private ticks: TickOptions = {};


    constructor() {

    }

    ngOnInit() {

    }

    ngAfterViewInit() {
        this._arr = [];
        this._arrLabel = [];
        var arrrr: number[] = [];
        var xpto: string[] = [];

        var title = this.chartName;

        setTimeout(() => {

            this.initialStocksValue.forEach(x => {
                this._arr.push(+getFixed(x.currentValue));
                this._arrLabel.push(x.lastUpdated);
            });

            this.max = Math.max(...this._arr);
            this.min = Math.min(...this._arr);
            this.diff = (this.max - this.min);

            if (this._arr.length > 15) {
                this._arr = this._arr.slice(this._arr.length - 15, this._arr.length);
                this._arrLabel = this._arrLabel.slice(this._arrLabel.length - 15, this._arrLabel.length);
            }


            this.chart = new Chart('myChart', {

                type: 'line',

                data: {
                    labels: this._arrLabel,
                    datasets: [{
                        label: title,
                        data: this._arr,
                        //   borderColor: '#ffcc00',
                        borderColor: '#0ba522',  //#008714 
                        pointBorderWidth: 5,
                        pointHitRadius: 12,
                        pointHoverRadius: 12,
                        fill: true,
                    }]
                },

                options: {
                    responsive: true,

                    legend: {
                        display: false
                    },

                    scales: {
                        xAxes: [{
                            display: true,
                            ticks: {
                                maxTicksLimit: 12,
                            },
                            stacked: true
                        }],
                        yAxes: [{
                            display: true,
                            ticks: {
                                max: this.max + this.diff,
                                min: this.min - this.diff,
                                maxTicksLimit: 12
                            },
                            stacked: false
                        }]
                    },
                    plugins: {
                        datalabels: {
                            backgroundColor: (context: any) => {
                                return context.dataset.backgroundColor;
                            },
                            borderRadius: 40,
                            color: 'black',
                            font: {
                                weight: 'bold',
                            },
                        },
                    },

                    animation: {
                        duration: 3                  // general animation time
                    }
                },

                plugins: {
                    afterInit: (chart: any, options: any) => {
                        document.getElementById("loadingGif")!.outerHTML = "";
                    },
                    datalabels: {
                        backgroundColor: (context: any) => {
                            return context.dataset.backgroundColor;
                        },
                        borderRadius: 40,
                        color: 'black',
                        font: {
                            weight: 'bold',
                        },
                    },

                    responsiveAnimationDuration: 3,    // animation duration after a resize
                }
            });


            setInterval(() => {
                var hasChanged = false;

                var stock = this.stocksValue.find(x => x.title == title);
                if (stock == null) {
                    return;
                }

                var value = getFixed(stock.currentValue);
                var intValue = +value;

                // first populate
                if (this._arr.length == 0) {
                    this._arr.push(intValue);
                    this._arrLabel.push(stock.lastUpdated);
                    this.max = intValue;
                    this.min = intValue;
                    this.diff = 0;
                    hasChanged = true;
                }
                this.ticks = this.chart.config.options!["scales"]!["yAxes"]![0]!["ticks"]!;


                var len = this._arr.length;
                if (intValue != this._arr[len - 1] || stock.lastUpdated != this._arrLabel[len - 1]) {
                    this._arr.push(intValue);
                    this._arrLabel.push(stock.lastUpdated);
                    hasChanged = true;
                }


                if (this._arr.length > 15) {
                    this._arr.shift();
                    this._arrLabel.shift();
                }


                if (this.max < stock.currentValue) {
                    this.max = stock.currentValue + (this.diff * 10);
                    this.ticks.max = this.max;
                    hasChanged = true;
                }
                if (this.min > stock.currentValue) {
                    this.min = stock.currentValue - (this.diff * 10);
                    this.ticks.min = this.min;
                    hasChanged = true;
                }


                if (hasChanged) {
                    this.chart.update();
                    this.chart.render();
                    this.chart.resize();
                }

            }, 500);

        }, 2000);

        function getFixed(val: number) {

            if (val >= 100)
                return val.toFixed(2);

            else
                if (val >= 10)
                    return val.toFixed(3);
                else
                    return val.toFixed(4);
        }



    }


}

