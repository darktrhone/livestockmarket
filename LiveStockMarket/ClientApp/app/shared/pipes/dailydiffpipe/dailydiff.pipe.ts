﻿import { Pipe, PipeTransform } from '@angular/core'

@Pipe({ name: 'DailyDiffPipe' })
export class DailyDiffPipe implements PipeTransform {
    transform(value: number): string {
       

        return value.toFixed(3) + '%';
      
    }
}