﻿import { Pipe, PipeTransform } from '@angular/core'

@Pipe({ name: 'CurrencyNotationPipe' })
export class CurrencyNotationPipe implements PipeTransform {
    transform(val: string): any {

        var number = +val;

        if (number <= 0.1) {
            return number.toFixed(5) + '€';
        }
        else if (number <= 10) {
            return number.toFixed(4) + '€';
        }
        else if (number <= 999) {
            return number.toFixed(3) + '€';
        }
        else if ( number <= 999999) {
            return (number / 1000).toFixed(2) + 'K€';
        }
        else if (number <= 999999999) {
            return (number / 1000000).toFixed(2) + 'M€';
        }
        else
            return (number / 1000000000).toFixed(2) + 'B€';
    }
}