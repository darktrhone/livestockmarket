﻿export class StockEntity {

    closingValue: number = 0;
    currentValue: number = 0;
    dayDiff: number = 0;
    title: string = "";
    fullName: string = "";
    quoteIndex: string = "";
    capital: string = "";
    lastUpdated: string = "";
    style: string = "";

    constructor() {

    }

    public static UpdateStock(oldStock: StockEntity, newStock: StockEntity) {
        oldStock.lastUpdated = newStock.lastUpdated;
        oldStock.currentValue = newStock.currentValue;
        oldStock.closingValue = newStock.closingValue;
        oldStock.dayDiff = ((newStock.currentValue / newStock.closingValue) * 100) - 100;
        oldStock.style = StockEntity.getDailyDiffStyle(oldStock.dayDiff);
    }

    static getDailyDiffStyle(dayDiff: number): string {

        if (dayDiff < -20)
            return "rgba(255, 0, 0,0.5)";

        else if (dayDiff < -15)
            return "rgba(255, 0, 0,0.4)";

        else if (dayDiff < -10)
            return "rgba(255, 0, 0,0.3)";

        else if (dayDiff < -5)
            return "rgba(255, 0, 0,0.2)";

        else if (dayDiff < -0.5)
            return "rgba(255, 0, 0,0.1)";

        else if (dayDiff >= -0.5 && dayDiff <= 0.5)
            return "rgba(240, 255, 0,0.05)";

        else if (dayDiff > 20)
            return "rgba(0,128,0,0.5)";

        else if (dayDiff > 15)
            return "rgba(0,128,0,0.4)";

        else if (dayDiff > 10)
            return "rgba(0,128,0,0.3)";

        else if (dayDiff > 5)
            return "rgba(0,128,0,0.2)";

        else if (dayDiff > 0.5)
            return "rgba(0,128,0,0.1)";
        else
            return "rgba(0, 0, 0,1)";

    }
}
