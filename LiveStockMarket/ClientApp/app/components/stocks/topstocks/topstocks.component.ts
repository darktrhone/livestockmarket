import { Component } from '@angular/core'
import { StockEntity } from '../../../shared/models/stockentity.model'
import { StockService } from '../../../services/stock.service'
import { Observable } from 'rxjs/Observable'

@Component({
    selector: 'topstocks',
    templateUrl: './topstocks.component.html',
    styleUrls: ['./topstocks.component.css']
})
export class TopStocksComponent {
    private stocks: StockEntity[] = [];
    private stocksReverted: StockEntity[] = [];

    constructor(private stockService: StockService) { }

    ngOnInit() {
         this.getRoutedStocks();
         this.getRoutedStocksReverted();
    }

    getRoutedStocks(): void {

        //this.stockService.getAllStocks()
        //    .subscribe(stocks => this.stocks = stocks);

    }
    getRoutedStocksReverted(): void {

        //this.stockService.getAllStocks()
        //    .subscribe(stocks => this.stocksReverted = stocks.reverse());

    }
}