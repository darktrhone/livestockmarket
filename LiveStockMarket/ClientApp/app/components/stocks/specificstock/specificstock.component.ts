import { Component, EventEmitter, Output, Input } from '@angular/core'
import { StockEntity } from '../../../shared/models/stockentity.model'
import { StockHubService } from '../../../services/stockhub.service'
import { Subscription } from 'rxjs/Subscription'
import { OnDestroy, OnInit } from '@angular/core/src/metadata/lifecycle_hooks'
import { ActivatedRoute, Params } from '@angular/router'
import { StockService } from '../../../services/stock.service'


@Component({
    selector: 'specificstock',
    templateUrl: './specificstock.component.html',
    styleUrls: ['./specificstock.component.css']
})
export class SpecificStockComponent implements OnDestroy, OnInit {

    @Output() onStocksUpdateEmitter: EventEmitter<StockEntity[]> = new EventEmitter();

    @Input() public routeName: string = "";

    private routeSubscription: Subscription;

    public stocks: StockEntity[] = [];

    public initialStocks: StockEntity[] = [];



    constructor(private stockHubService: StockHubService, private route: ActivatedRoute, private stockService: StockService) {

        this.routeSubscription = this.route.params.subscribe(
            (params: Params) => {
                this.routeName = params["stockName"];
            });

        this.connectStocks("AllStocks");
    }


    ngOnInit() {

        this.stockService.getStockByID(this.routeName).subscribe((stocks: StockEntity[]) => {
            this.initialStocks.push(...stocks);
        });
    }

    ngOnDestroy() {
        console.log("Disconnect:", "AllStocks");

        this.stockHubService.disconnectMethod("AllStocks");

        this.routeSubscription.unsubscribe();

        this.onStocksUpdateEmitter.unsubscribe();
    }


    connectStocks(method: string): void {

        this.stockHubService.connectMethod(method, this.onStocksUpdateEmitter);

        this.onStocksUpdateEmitter.subscribe((updatedStocks: StockEntity[]) => {
            if (this.stocks.length == 0) {
                this.stocks = updatedStocks;
            }

            updatedStocks.forEach(newStock => {
                var currentStock = this.stocks.find(y => y.title == newStock.title)!;
                StockEntity.UpdateStock(currentStock, newStock);
            });
        });
    }
}

