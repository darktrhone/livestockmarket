import { Component, Input } from '@angular/core'
import { ActivatedRoute, Params } from '@angular/router'
import { Subscription } from 'rxjs/Subscription'
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks'

@Component({
    selector: 'stockheader',
    templateUrl: './stockheader.component.html',
    styleUrls: ['./stockheader.component.css']
})
export class StockHeaderComponent implements OnDestroy {
    @Input() public routeName: string = "";

    private routeSubscription: Subscription = new Subscription(); 

    constructor(private route: ActivatedRoute) {
        if (this.routeName == null || this.routeName.length == 0) {
            this.routeSubscription = this.route.params.subscribe(
                (params: Params) => {
                    this.routeName = params["stockName"];
                });
        }

        if (this.routeName == null || this.routeName.length == 0) {
            this.routeName = "Current Stocks";
        }
     } 

    ngOnDestroy() {
        if (this.routeSubscription) {
            this.routeSubscription.unsubscribe();
        }
    }

}
