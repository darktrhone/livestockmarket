import { Component, Output, EventEmitter, OnDestroy } from '@angular/core'
import { StockEntity } from '../../shared/models/stockentity.model'
import { StockService } from '../../services/stock.service'
import { Observable } from 'rxjs/Observable'
import { StockHubService } from '../../services/stockhub.service'
import { ActivatedRoute, Params } from '@angular/router'
import { Subscription } from 'rxjs/Subscription'


@Component({
    selector: 'stocks',
    templateUrl: './stocks.component.html',
    styleUrls: ['./stocks.component.css']
})
export class StocksComponent implements OnDestroy {

    @Output() onStocksUpdateEmitter: EventEmitter<StockEntity[]> = new EventEmitter();

    public stocks: StockEntity[] = [];

    public initialStocks: StockEntity[] = [];

    private routeSubscription: Subscription;

    private methodName: string = "";

    public shouldRenderChart: boolean = true;

    constructor(private stockHubService: StockHubService, private route: ActivatedRoute) {
        this.routeSubscription = this.route.params.subscribe(
            (params: Params) => {
                this.methodName = params["stockName"] ? params["stockName"] : 'AllStocks';
                this.shouldRenderChart = this.methodName != 'AllStocks' && this.methodName != 'CryptoCurrency' && this.methodName != 'Currency';
                this.connectStocks(this.methodName);
            });

    }

    ngOnDestroy() {
        console.log("Disconnect:", this.methodName);

        this.stockHubService.disconnectMethod(this.methodName);

        this.onStocksUpdateEmitter.unsubscribe();

        this.routeSubscription.unsubscribe();

    }


    connectStocks(method: string): void {
        this.stockHubService.connectMethod(method, this.onStocksUpdateEmitter);

        this.onStocksUpdateEmitter.subscribe((updatedStocks: StockEntity[]) => {
            if (this.stocks.length == 0) {
                this.stocks = updatedStocks;
            }

            updatedStocks.forEach(newStock => {
                var currentStock = this.stocks.find(y => y.title == newStock.title)!;
                StockEntity.UpdateStock(currentStock, newStock);
            });
        });
    }


}
