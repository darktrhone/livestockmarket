import { NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { CommonModule } from '@angular/common'
import { HttpModule } from '@angular/http'
import { Routes, RouterModule } from '@angular/router'
import { SpecificStockComponent } from './components/stocks/specificstock/specificstock.component'
import { StocksComponent } from './components/stocks/stocks.component'
import { TopStocksComponent } from './components/stocks/topstocks/topstocks.component'
import { StockTableComponent } from './shared/components/stocktable/stocktable.component'
import { StockHeaderComponent } from './components/stocks/stockheader/stockheader.component'
import { StockChartComponent } from './shared/components/stockchart/stockchart.component'
import { StockService } from './services/stock.service'
import { StockHubService } from './services/stockhub.service'
import { CurrencyNotationPipe } from './shared/pipes/currencynotationpipe/currencynotation.pipe'
import { DailyDiffPipe } from './shared/pipes/dailydiffpipe/dailydiff.pipe'


export const routes: Routes = [
    { path: '', component: StocksComponent },
    { path: 'top', component: TopStocksComponent },
    { path: ':stockName', component: StocksComponent },
    { path: 'detail/:stockName', component: SpecificStockComponent }
];


@NgModule({
    declarations: [
        StocksComponent,
        TopStocksComponent,
        StockTableComponent,
        SpecificStockComponent,
        StockHeaderComponent,
        StockChartComponent,
        CurrencyNotationPipe,
        DailyDiffPipe
    ],
    imports: [
        CommonModule,
        HttpModule,
        FormsModule,
        RouterModule.forChild(routes),
    ],
    providers: [
        StockService,
        StockHubService
    ]
})
export class StockModule {
}
