﻿using LiveStockMarket.Infrastructure.External.Models;
using LiveStockMarket.Infrastructure.External.Providers;
using System.Threading.Tasks;

namespace LiveStockMarket.Infrastructure.Domain.Services
{
    public class BloombergStockService<T,TExternal> : BaseStockService<T>, IBloombergStockService<T>
        where T : BloombergStockEntity
        where TExternal : BloombergExternalStockEntity
    {

        protected IBaseStockProvider<TExternal> _provider;

        public void StockService(IBloombergStockProvider<TExternal> provider)
        {
            _provider = provider;
        }

 
       public override async Task<T> ExecuteRequest(string url)
       {
            var result = await _provider?.ExecuteRequest();

            return _mapper.Map<T>(result);
        }
    }
}
 