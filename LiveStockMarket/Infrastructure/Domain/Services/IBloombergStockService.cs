﻿
using LiveStockMarket.Infrastructure.Domain.Models;
using LiveStockMarket.Infrastructure.External.Models;


namespace LiveStockMarket.Infrastructure.Domain.Services
{

    public interface IBloombergStockService<T> : IBaseStockService<T>
        where T : BloombergStockEntity
    {

    }
}
