﻿using AutoMapper;
using LiveStockMarket.Infrastructure.Domain.Models;
using LiveStockMarket.Infrastructure.External.Models;
using LiveStockMarket.Infrastructure.External.Providers;
using System.Threading.Tasks;

namespace LiveStockMarket.Infrastructure.Domain.Services
{
    public abstract class BaseStockService<T> 
        where T : BaseStockEntity

    {
        protected IMapper _mapper;
        

        public void StockService(IMapper mapper)
        {
            _mapper = mapper;
        }



        public abstract Task<T> ExecuteRequest(string url); 


    }
}
