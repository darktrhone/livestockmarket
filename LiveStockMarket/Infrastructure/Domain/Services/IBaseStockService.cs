﻿using LiveStockMarket.Infrastructure.Domain.Models;
using System.Threading.Tasks;

namespace LiveStockMarket.Infrastructure.Domain.Services
{

    public interface IBaseStockService<T>  where T : BaseStockEntity 
    {
        Task<T> ExecuteRequest(string url);

    }
}
