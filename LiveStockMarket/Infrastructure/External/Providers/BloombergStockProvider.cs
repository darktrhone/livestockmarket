﻿using LiveStockMarket.Infrastructure.External.Models;

namespace LiveStockMarket.Infrastructure.External.Providers
{
    public class BloombergStockProvider : BaseStockProvider<BloombergExternalStockEntity>, IBloombergStockProvider<BloombergExternalStockEntity>      
    {

        public BloombergStockProvider(string url) : base(url)
        {
        }

    }
}
