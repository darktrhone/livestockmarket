﻿using LiveStockMarket.Infrastructure.External.Models;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace LiveStockMarket.Infrastructure.External.Providers
{
    public class BaseStockProvider<T> : IBaseStockProvider<T>
     where T : BaseExternalStockEntity
    {

        string _connectionUrl = string.Empty;

        public BaseStockProvider()
        {
        }

        public BaseStockProvider(string url)
        {
            ConfigureEndpoint(url);
        }

        public void ConfigureEndpoint(string url)
        {
            this._connectionUrl = url;
        }


        public async Task<T> ExecuteRequest()
        {
            try
            {
                HttpWebRequest request = WebRequest.CreateHttp(_connectionUrl);

                var ws = await request.GetResponseAsync();

                return ws.ResponseUri as T;
            }
            catch (Exception e)
            {
                throw e;
                //  return default(T);
            }

        }
 
    }
}
