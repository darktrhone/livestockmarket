﻿using LiveStockMarket.Infrastructure.External.Models;
using System.Threading.Tasks;

namespace LiveStockMarket.Infrastructure.External.Providers
{
    public interface IBloombergStockProvider<TExternal>: IBaseStockProvider<TExternal>
        where TExternal : BloombergExternalStockEntity
    {
 
        //void ConfigureEndpoint(string url);
 
        //Task<BloombergExternalStockEntity> ExecuteRequest();

    }
}
