﻿using LiveStockMarket.Infrastructure.External.Models;
using System.Threading.Tasks;

namespace LiveStockMarket.Infrastructure.External.Providers
{
    public interface IBaseStockProvider<T> where T : BaseExternalStockEntity
    {
 
        void ConfigureEndpoint(string url);
 
        Task<T> ExecuteRequest();

    }
}
