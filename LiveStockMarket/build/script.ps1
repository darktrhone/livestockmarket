ECHO "Listing containers to remove." 
docker ps -a --format '{{.Image}}'

ECHO "Stopping containers." 
docker stop $(docker ps -aq)

ECHO "Removing containers." 
docker rm $(docker ps -aq) 

ECHO "Cleaning networks."
ECHO 'y' | docker network prune 
ECHO "Create internal network."
docker network create redis_cluster
 
 #Docker pull Redis
 #verificar se drive C est� partilhada
ECHO "Initiating redis container."
docker run -d -v C:/Users/rui.silveira/.docker/redis/redis.conf:/usr/local/etc/redis/redis.conf --name redis-1 -p 6379:6379 --net redis_cluster redis redis-server /usr/local/etc/redis/redis.conf
ECHO "created redis-1"
docker run -d -v C:/Users/rui.silveira/.docker/redis/redis.conf:/usr/local/etc/redis/redis.conf --name redis-2 -p 6380:6379 --net redis_cluster redis redis-server /usr/local/etc/redis/redis.conf
ECHO "created redis-2"
docker run -d -v C:/Users/rui.silveira/.docker/redis/redis.conf:/usr/local/etc/redis/redis.conf --name redis-3 -p 6381:6379 --net redis_cluster redis redis-server /usr/local/etc/redis/redis.conf  
ECHO "created redis-3"
docker run -d -v C:/Users/rui.silveira/.docker/redis/redis.conf:/usr/local/etc/redis/redis.conf --name redis-4 -p 6382:6379 --net redis_cluster redis redis-server /usr/local/etc/redis/redis.conf  
ECHO "created redis-4"
docker run -d -v C:/Users/rui.silveira/.docker/redis/redis.conf:/usr/local/etc/redis/redis.conf --name redis-5 -p 6383:6379 --net redis_cluster redis redis-server /usr/local/etc/redis/redis.conf   
ECHO "created redis-5"
docker run -d -v C:/Users/rui.silveira/.docker/redis/redis.conf:/usr/local/etc/redis/redis.conf --name redis-6 -p 6384:6379 --net redis_cluster redis redis-server /usr/local/etc/redis/redis.conf   
ECHO "created redis-6"

  
ECHO "Initiating redis cluster." 
docker run -i --rm --net redis_cluster ruby sh -c 'gem install redis && wget http://download.redis.io/redis-stable/src/redis-trib.rb && ruby redis-trib.rb create --replicas 1 172.24.0.2:6379 172.24.0.3:6379 172.24.0.4:6379 172.24.0.5:6379 172.24.0.6:6379 172.24.0.7:6379'
ECHO "Initiated redis cluster." 

 
 ECHO Pulling Windows Server Linux
docker pull microsoft/mssql-server-linux:2017-latest

ECHO Creating Windows Server Linux
docker run --net redis_cluster --name SQLServer2017 -e "ACCEPT_EULA=Y" -e "MSSQL_SA_PASSWORD=Admin!2018" -e "MSSQL_PID=Developer" --cap-add SYS_PTRACE -p 7101:1433 -d microsoft/mssql-server-linux:2017-latest




   docker rm -f livestockmarket
ECHO "Building LiveStockMarket"
cd C:\Users\rui.silveira\Tools\Repositories\LiveStockMarket\LiveStockMarket 
docker build -t livestockmarket .
ECHO "Initiating LiveStockMarket"
docker run -d  --net redis_cluster -p 4200:443 --name livestockmarket livestockmarket
#--net redis_cluster  

docker rm -f stockmarketemitter
ECHO "Building StockMarketEmitter"
cd C:\Users\rui.silveira\Tools\Repositories\LiveStockMarket\StockMarketEmitter
docker build -t stockmarketemitter . 
ECHO "Initiating StockMarketEmitter"
docker run -d --net redis_cluster --name stockmarketemitter stockmarketemitter
 
 docker rm -f stockmarketsyncer
ECHO "Building StockMarketSyncer"
cd C:\Users\rui.silveira\Tools\Repositories\LiveStockMarket\StockMarketSyncer 
docker build -t stockmarketsyncer . 
ECHO "Initiating StockMarketSyncer"
docker run -d --net redis_cluster  --name stockmarketsyncer stockmarketsyncer
   
   
ECHO "Finished."



docker exec -it redis-3 redis-cli