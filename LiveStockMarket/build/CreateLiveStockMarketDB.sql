create database LiveStockMarket

use LiveStockMarket

 
create table Stocks (
ID int primary key identity,
Title nvarchar(30),
FullName nvarchar(100),
CurrentValue decimal(20,5),
ClosingValue decimal(20,5),
Capital bigint, 
DayDiff decimal(6,5),
LastUpdated datetime,
QuoteIndex nvarchar(15)
);


insert into Stocks values 
						('WMT','WallMart Stores',80.48, 24261100200,24261100200,0.10, GETDATE(), ''),
                        ('GS', 'Goldman Sachs Group', 229.79, 9258100200,9258100200, 0.10, GETDATE(), ''),
                       ('VRSN', 'VeriSign', 99.31, 992100200,992100200,0.10, GETDATE(), ''),
                       ('NOV', 'National Oilwell Varco', 33.18, 1261100200,1261100200,0.10, GETDATE(), ''),
                       ('UPS', 'United Parcel Service', 112.05, 9692100200,9692100200,0.10, GETDATE(), ''),
                       ('LEE', 'Lee Enterprises', 2.10, 11896100,11896100,0.10, GETDATE(), ''),
                       ('NDX', 'NASDAQ 100', 5899.91, 724101002000,724101002000,0.10, GETDATE(), ''),
                       ('AAPL', 'Apple Inc.', 156.39, 80850100200,80850100200, 0.10, GETDATE(),'TECH' ),
                       ('HPQ', 'HP Inc.', 19.37, 3261100200,3261100200,0.10, GETDATE(), 'TECH'),
                       ('IBM', 'International Business Machines', 145.16, 13528100200,13528100200,0.10, GETDATE(), 'TECH'),
                       ('INTC', 'Intel Corporation', 36.30, 170.571,170.571, 0.10, GETDATE(), 'TECH'),
                       ('NAB.AX', 'Nat. Bank FPO', 29.93, 79.7710,79.7710,0.10, GETDATE(), ''),
                       ('MCD', 'McDonald''s Corporation', 153.82, 12537100200,12537100200,0.10, GETDATE(), ''),
                       ('V', 'Visa Inc.', 100.89, 23068100200,23068100200,0.10, GETDATE(), ''),
                       ('DIS', 'Walt Disney Company', 107.69, 16852100200,16852100200,0.10, GETDATE(), ''),
                       ('MSFT', 'Microsoft Corporation', 72.68, 56022100200,56022100200, 0.10, GETDATE(), 'TECH'),
                       ('FTSE', 'FTSE 100', 7511.71, 2756022100200,2756022100200, 0.10, GETDATE(), ''),
                       ('VOD.L', 'Vodafone Group', 225.00, 5990100200,5990100200,0.10, GETDATE(), ''),
                       ('TSCO.L', 'Tesco PLC Ord 5P', 180.15, 1469100200,1469100200,0.10, GETDATE(), ''),
                       ('SBRY.L', 'Sainsbury', 252.50, 551100200,551100200,0.10, GETDATE(), '');

					   create table StocksHistory (
ID int primary key identity,
StockID int,
OpeningValue decimal(20,5),
ClosingDate datetime DEFAULT GETDATE(),
CONSTRAINT FK_StockID FOREIGN KEY (StockID) REFERENCES Stocks (ID) 
); 

 

insert into Stocks  (FullName,Title,CurrentValue,Capital,ClosingValue,DayDiff,LastUpdated,QuoteIndex) values
('BITCOIN','BTC','9348.18907','158290588811',0.1,0.1,'2018-04-30 14:29:02.157','CryptoCurrency'),
('ETHEREUM','ETH','686.00093','68190925705',0.1,0.1,'2018-04-30 14:29:02.157','CryptoCurrency'),
('RIPPLE','XRP','0.85255','33322070403',0.1,0.1,'2018-04-30 14:29:02.157','CryptoCurrency'),
('BITCOIN CASH','BCH','1407.46833','23996943182',0.1,0.1,'2018-04-30 14:29:02.157','CryptoCurrency'),
('EOS','EOS','19.02513','15707681144',0.1,0.1,'2018-04-30 14:29:02.157','CryptoCurrency'),
('CARDANO','ADA','0.35018','9135740356',0.1,0.1,'2018-04-30 14:29:02.157','CryptoCurrency'),
('LITECOIN','LTC','151.67457','8554527061',0.1,0.1,'2018-04-30 14:29:02.157','CryptoCurrency'),
('STELLAR','XLM','0.43880','8141109271',0.1,0.1,'2018-04-30 14:29:02.157','CryptoCurrency'),
('TRON','TRX','0.09531','6266452521',0.1,0.1,'2018-04-30 14:29:02.157','CryptoCurrency'),
('NEO','NEO','87.20506','5672634500',0.1,0.1,'2018-04-30 14:29:02.157','CryptoCurrency'),
('IOTA','MIOTA','1.98680','5529486364',0.1,0.1,'2018-04-30 14:29:02.157','CryptoCurrency'),
('MONERO','XMR','243.94228','3895522294',0.1,0.1,'2018-04-30 14:29:02.157','CryptoCurrency'),
('DASH','DASH','483.32555','3860387629',0.1,0.1,'2018-04-30 14:29:02.157','CryptoCurrency'),
('NEM','XEM','0.41355','3709116000',0.1,0.1,'2018-04-30 14:29:02.157','CryptoCurrency'),
('VECHAIN','VEN','4.70080','2490610517',0.1,0.1,'2018-04-30 14:29:02.157','CryptoCurrency'),
('TETHER','USDT','1.00613','2420645668',0.1,0.1,'2018-04-30 14:29:02.157','CryptoCurrency'),
('ETHEREUM CLASSIC','ETC','22.15099','2262398091',0.1,0.1,'2018-04-30 14:29:02.157','CryptoCurrency'),
('QTUM','QTUM','24.07099','2127396664',0.1,0.1,'2018-04-30 14:29:02.157','CryptoCurrency'),
('OMISEGO','OMG','17.54475','1791163115',0.1,0.1,'2018-04-30 14:29:02.157','CryptoCurrency'),
('ICON','ICX','4.46735','1743838293',0.1,0.1,'2018-04-30 14:29:02.157','CryptoCurrency'),
('BINANCE COIN','BNB','14.45366','1657647172',0.1,0.1,'2018-04-30 14:29:02.157','CryptoCurrency'),
('LISK','LSK','12.69351','1336760341',0.1,0.1,'2018-04-30 14:29:02.157','CryptoCurrency'),
('BITCOIN GOLD','BTG','74.95200','1274090680',0.1,0.1,'2018-04-30 14:29:02.157','CryptoCurrency'),
('NANO','NANO','8.35149','1120939241',0.1,0.1,'2018-04-30 14:29:02.157','CryptoCurrency'),
('AETERNITY','AE','4.80762','1113735328',0.1,0.1,'2018-04-30 14:29:02.157','CryptoCurrency')
