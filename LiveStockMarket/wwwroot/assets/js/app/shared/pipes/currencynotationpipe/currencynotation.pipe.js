var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Pipe } from '@angular/core';
var CurrencyNotationPipe = /** @class */ (function () {
    function CurrencyNotationPipe() {
    }
    CurrencyNotationPipe.prototype.transform = function (val) {
        var number = +val;
        if (number <= 0.1) {
            return number.toFixed(5) + '€';
        }
        else if (number <= 10) {
            return number.toFixed(4) + '€';
        }
        else if (number <= 999) {
            return number.toFixed(3) + '€';
        }
        else if (number <= 999999) {
            return (number / 1000).toFixed(2) + 'K€';
        }
        else if (number <= 999999999) {
            return (number / 1000000).toFixed(2) + 'M€';
        }
        else
            return (number / 1000000000).toFixed(2) + 'B€';
    };
    CurrencyNotationPipe = __decorate([
        Pipe({ name: 'CurrencyNotationPipe' })
    ], CurrencyNotationPipe);
    return CurrencyNotationPipe;
}());
export { CurrencyNotationPipe };
//# sourceMappingURL=currencynotation.pipe.js.map