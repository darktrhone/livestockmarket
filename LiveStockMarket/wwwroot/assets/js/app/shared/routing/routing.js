var CustomReuseStrategy = /** @class */ (function () {
    function CustomReuseStrategy() {
        this.routesToCache = ["empty"];
        this.storedRouteHandles = new Map();
    }
    // Decides if the route should be stored
    CustomReuseStrategy.prototype.shouldDetach = function (route) {
        return false;
    };
    //Store the information for the route we're destructing
    CustomReuseStrategy.prototype.store = function (route, handle) {
        return;
    };
    //Return true if we have a stored route object for the next route
    CustomReuseStrategy.prototype.shouldAttach = function (route) {
        return false;
    };
    //If we returned true in shouldAttach(), now return the actual route data for restoration
    CustomReuseStrategy.prototype.retrieve = function (route) {
        return null;
    };
    CustomReuseStrategy.prototype.shouldReuseRoute = function (future, curr) {
        console.log("false;");
        return false;
    };
    return CustomReuseStrategy;
}());
export { CustomReuseStrategy };
//# sourceMappingURL=routing.js.map