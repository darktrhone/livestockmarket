var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input } from '@angular/core';
import { Chart } from 'chart.js';
import '../../../../../wwwroot/assets/js/chartjs-plugin-datalabels';
var StockChartComponent = /** @class */ (function () {
    function StockChartComponent() {
        this.initialStocksValue = [];
        this.stocksValue = [];
        this.chartName = "";
        this.chart = new Chart("", {});
        this._arrLabel = [];
        this._arr = [];
        this.max = 0;
        this.min = 0;
        this.diff = 0;
        this.ticks = {};
    }
    StockChartComponent.prototype.ngOnInit = function () {
    };
    StockChartComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this._arr = [];
        this._arrLabel = [];
        var arrrr = [];
        var xpto = [];
        var title = this.chartName;
        setTimeout(function () {
            _this.initialStocksValue.forEach(function (x) {
                _this._arr.push(+getFixed(x.currentValue));
                _this._arrLabel.push(x.lastUpdated);
            });
            _this.max = Math.max.apply(Math, _this._arr);
            _this.min = Math.min.apply(Math, _this._arr);
            _this.diff = (_this.max - _this.min);
            if (_this._arr.length > 15) {
                _this._arr = _this._arr.slice(_this._arr.length - 15, _this._arr.length);
                _this._arrLabel = _this._arrLabel.slice(_this._arrLabel.length - 15, _this._arrLabel.length);
            }
            _this.chart = new Chart('myChart', {
                type: 'line',
                data: {
                    labels: _this._arrLabel,
                    datasets: [{
                            label: title,
                            data: _this._arr,
                            //   borderColor: '#ffcc00',
                            borderColor: '#0ba522',
                            pointBorderWidth: 5,
                            pointHitRadius: 12,
                            pointHoverRadius: 12,
                            fill: true,
                        }]
                },
                options: {
                    responsive: true,
                    legend: {
                        display: false
                    },
                    scales: {
                        xAxes: [{
                                display: true,
                                ticks: {
                                    maxTicksLimit: 12,
                                },
                                stacked: true
                            }],
                        yAxes: [{
                                display: true,
                                ticks: {
                                    max: _this.max + _this.diff,
                                    min: _this.min - _this.diff,
                                    maxTicksLimit: 12
                                },
                                stacked: false
                            }]
                    },
                    plugins: {
                        datalabels: {
                            backgroundColor: function (context) {
                                return context.dataset.backgroundColor;
                            },
                            borderRadius: 40,
                            color: 'black',
                            font: {
                                weight: 'bold',
                            },
                        },
                    },
                    animation: {
                        duration: 3 // general animation time
                    }
                },
                plugins: {
                    afterInit: function (chart, options) {
                        document.getElementById("loadingGif").outerHTML = "";
                    },
                    datalabels: {
                        backgroundColor: function (context) {
                            return context.dataset.backgroundColor;
                        },
                        borderRadius: 40,
                        color: 'black',
                        font: {
                            weight: 'bold',
                        },
                    },
                    responsiveAnimationDuration: 3,
                }
            });
            setInterval(function () {
                var hasChanged = false;
                var stock = _this.stocksValue.find(function (x) { return x.title == title; });
                if (stock == null) {
                    return;
                }
                var value = getFixed(stock.currentValue);
                var intValue = +value;
                // first populate
                if (_this._arr.length == 0) {
                    _this._arr.push(intValue);
                    _this._arrLabel.push(stock.lastUpdated);
                    _this.max = intValue;
                    _this.min = intValue;
                    _this.diff = 0;
                    hasChanged = true;
                }
                _this.ticks = _this.chart.config.options["scales"]["yAxes"][0]["ticks"];
                var len = _this._arr.length;
                if (intValue != _this._arr[len - 1] || stock.lastUpdated != _this._arrLabel[len - 1]) {
                    _this._arr.push(intValue);
                    _this._arrLabel.push(stock.lastUpdated);
                    hasChanged = true;
                }
                if (_this._arr.length > 15) {
                    _this._arr.shift();
                    _this._arrLabel.shift();
                }
                if (_this.max < stock.currentValue) {
                    _this.max = stock.currentValue + (_this.diff * 10);
                    _this.ticks.max = _this.max;
                    hasChanged = true;
                }
                if (_this.min > stock.currentValue) {
                    _this.min = stock.currentValue - (_this.diff * 10);
                    _this.ticks.min = _this.min;
                    hasChanged = true;
                }
                if (hasChanged) {
                    _this.chart.update();
                    _this.chart.render();
                    _this.chart.resize();
                }
            }, 500);
        }, 2000);
        function getFixed(val) {
            if (val >= 100)
                return val.toFixed(2);
            else if (val >= 10)
                return val.toFixed(3);
            else
                return val.toFixed(4);
        }
    };
    __decorate([
        Input(),
        __metadata("design:type", Array)
    ], StockChartComponent.prototype, "initialStocksValue", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Array)
    ], StockChartComponent.prototype, "stocksValue", void 0);
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], StockChartComponent.prototype, "chartName", void 0);
    StockChartComponent = __decorate([
        Component({
            selector: 'stockchart',
            templateUrl: './stockchart.component.html',
            styleUrls: ['./stockchart.component.css']
        }),
        __metadata("design:paramtypes", [])
    ], StockChartComponent);
    return StockChartComponent;
}());
export { StockChartComponent };
//# sourceMappingURL=stockchart.component.js.map