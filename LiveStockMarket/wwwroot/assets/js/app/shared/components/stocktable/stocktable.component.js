var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input } from '@angular/core';
var StockTableComponent = /** @class */ (function () {
    function StockTableComponent() {
        this.stocksValue = [];
    }
    __decorate([
        Input(),
        __metadata("design:type", Array)
    ], StockTableComponent.prototype, "stocksValue", void 0);
    StockTableComponent = __decorate([
        Component({
            selector: 'stocktable',
            templateUrl: './stocktable.component.html',
            styleUrls: ['./stocktable.component.css']
        })
    ], StockTableComponent);
    return StockTableComponent;
}());
export { StockTableComponent };
//# sourceMappingURL=stocktable.component.js.map