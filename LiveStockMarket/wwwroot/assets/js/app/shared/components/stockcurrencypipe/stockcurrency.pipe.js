var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Pipe } from '@angular/core';
var StockCurrencyPipe = /** @class */ (function () {
    function StockCurrencyPipe() {
    }
    StockCurrencyPipe.prototype.transform = function (val) {
        if (val >= 100)
            return val.toFixed(2);
        else if (val >= 10)
            return val.toFixed(3);
        else
            return val.toFixed(4);
    };
    StockCurrencyPipe = __decorate([
        Pipe({ name: 'StockCurrencyPipe' })
    ], StockCurrencyPipe);
    return StockCurrencyPipe;
}());
export { StockCurrencyPipe };
//# sourceMappingURL=stockcurrency.pipe.js.map