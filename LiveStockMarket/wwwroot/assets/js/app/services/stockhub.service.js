var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { HubConnection } from '@aspnet/signalr';
var StockHubService = /** @class */ (function () {
    function StockHubService() {
        this.hubConnection = new HubConnection("/StockHub");
        this.startConnection();
    }
    StockHubService.prototype.ngOnInit = function () { };
    StockHubService.prototype.connectMethod = function (method, emitter) {
        this.hubConnection.on(method, function (obj) {
            emitter.emit(obj);
        });
    };
    StockHubService.prototype.disconnectMethod = function (method) {
        this.hubConnection.off(method);
    };
    StockHubService.prototype.startConnection = function () {
        this.hubConnection
            .start()
            .then(function () { return console.log('Connection started!'); })
            .catch(function (err) { return console.log("Error -- " + err); });
    };
    StockHubService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [])
    ], StockHubService);
    return StockHubService;
}());
export { StockHubService };
//# sourceMappingURL=stockhub.service.js.map