var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Http, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
var StockService = /** @class */ (function () {
    function StockService(http) {
        this.http = http;
    }
    StockService.prototype.getStockByID = function (ID) {
        return this.http.get("/Api/GetStockByID", new RequestOptions({ params: { ID: ID } })).map(function (res) {
            return res.json().map(function (item) {
                return item;
            });
        });
    };
    var _a;
    StockService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [typeof (_a = typeof Http !== "undefined" && Http) === "function" ? _a : Object])
    ], StockService);
    return StockService;
}());
export { StockService };
//# sourceMappingURL=stock.service.js.map