var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
var StockHeaderComponent = /** @class */ (function () {
    function StockHeaderComponent(route) {
        var _this = this;
        this.route = route;
        this.routeName = "";
        this.routeSubscription = new Subscription();
        if (this.routeName == null || this.routeName.length == 0) {
            this.routeSubscription = this.route.params.subscribe(function (params) {
                _this.routeName = params["stockName"];
            });
        }
        if (this.routeName == null || this.routeName.length == 0) {
            this.routeName = "Current Stocks";
        }
    }
    StockHeaderComponent.prototype.ngOnDestroy = function () {
        if (this.routeSubscription) {
            this.routeSubscription.unsubscribe();
        }
    };
    var _a;
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], StockHeaderComponent.prototype, "routeName", void 0);
    StockHeaderComponent = __decorate([
        Component({
            selector: 'stockheader',
            templateUrl: './stockheader.component.html',
            styleUrls: ['./stockheader.component.css']
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof ActivatedRoute !== "undefined" && ActivatedRoute) === "function" ? _a : Object])
    ], StockHeaderComponent);
    return StockHeaderComponent;
}());
export { StockHeaderComponent };
//# sourceMappingURL=stockheader.component.js.map