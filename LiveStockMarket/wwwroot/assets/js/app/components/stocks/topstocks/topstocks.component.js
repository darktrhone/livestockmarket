var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { StockService } from '../../../services/stock.service';
var TopStocksComponent = /** @class */ (function () {
    function TopStocksComponent(stockService) {
        this.stockService = stockService;
        this.stocks = [];
        this.stocksReverted = [];
    }
    TopStocksComponent.prototype.ngOnInit = function () {
        this.getRoutedStocks();
        this.getRoutedStocksReverted();
    };
    TopStocksComponent.prototype.getRoutedStocks = function () {
        //this.stockService.getAllStocks()
        //    .subscribe(stocks => this.stocks = stocks);
    };
    TopStocksComponent.prototype.getRoutedStocksReverted = function () {
        //this.stockService.getAllStocks()
        //    .subscribe(stocks => this.stocksReverted = stocks.reverse());
    };
    TopStocksComponent = __decorate([
        Component({
            selector: 'topstocks',
            templateUrl: './topstocks.component.html',
            styleUrls: ['./topstocks.component.css']
        }),
        __metadata("design:paramtypes", [StockService])
    ], TopStocksComponent);
    return TopStocksComponent;
}());
export { TopStocksComponent };
//# sourceMappingURL=topstocks.component.js.map