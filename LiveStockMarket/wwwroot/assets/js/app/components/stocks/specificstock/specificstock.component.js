var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, EventEmitter, Output, Input } from '@angular/core';
import { StockEntity } from '../../../shared/models/stockentity.model';
import { StockHubService } from '../../../services/stockhub.service';
import { ActivatedRoute } from '@angular/router';
import { StockService } from '../../../services/stock.service';
var SpecificStockComponent = /** @class */ (function () {
    function SpecificStockComponent(stockHubService, route, stockService) {
        var _this = this;
        this.stockHubService = stockHubService;
        this.route = route;
        this.stockService = stockService;
        this.onStocksUpdateEmitter = new EventEmitter();
        this.routeName = "";
        this.stocks = [];
        this.initialStocks = [];
        this.routeSubscription = this.route.params.subscribe(function (params) {
            _this.routeName = params["stockName"];
        });
        this.connectStocks("AllStocks");
    }
    SpecificStockComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.stockService.getStockByID(this.routeName).subscribe(function (stocks) {
            var _a;
            (_a = _this.initialStocks).push.apply(_a, stocks);
        });
    };
    SpecificStockComponent.prototype.ngOnDestroy = function () {
        console.log("Disconnect:", "AllStocks");
        this.stockHubService.disconnectMethod("AllStocks");
        this.routeSubscription.unsubscribe();
        this.onStocksUpdateEmitter.unsubscribe();
    };
    SpecificStockComponent.prototype.connectStocks = function (method) {
        var _this = this;
        this.stockHubService.connectMethod(method, this.onStocksUpdateEmitter);
        this.onStocksUpdateEmitter.subscribe(function (updatedStocks) {
            if (_this.stocks.length == 0) {
                _this.stocks = updatedStocks;
            }
            updatedStocks.forEach(function (newStock) {
                var currentStock = _this.stocks.find(function (y) { return y.title == newStock.title; });
                StockEntity.UpdateStock(currentStock, newStock);
            });
        });
    };
    var _a, _b;
    __decorate([
        Output(),
        __metadata("design:type", typeof (_a = typeof EventEmitter !== "undefined" && EventEmitter) === "function" ? _a : Object)
    ], SpecificStockComponent.prototype, "onStocksUpdateEmitter", void 0);
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], SpecificStockComponent.prototype, "routeName", void 0);
    SpecificStockComponent = __decorate([
        Component({
            selector: 'specificstock',
            templateUrl: './specificstock.component.html',
            styleUrls: ['./specificstock.component.css']
        }),
        __metadata("design:paramtypes", [StockHubService, typeof (_b = typeof ActivatedRoute !== "undefined" && ActivatedRoute) === "function" ? _b : Object, StockService])
    ], SpecificStockComponent);
    return SpecificStockComponent;
}());
export { SpecificStockComponent };
//# sourceMappingURL=specificstock.component.js.map