var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Output, EventEmitter } from '@angular/core';
import { StockEntity } from '../../shared/models/stockentity.model';
import { StockHubService } from '../../services/stockhub.service';
import { ActivatedRoute } from '@angular/router';
var StocksComponent = /** @class */ (function () {
    function StocksComponent(stockHubService, route) {
        var _this = this;
        this.stockHubService = stockHubService;
        this.route = route;
        this.onStocksUpdateEmitter = new EventEmitter();
        this.stocks = [];
        this.initialStocks = [];
        this.methodName = "";
        this.shouldRenderChart = true;
        this.routeSubscription = this.route.params.subscribe(function (params) {
            _this.methodName = params["stockName"] ? params["stockName"] : 'AllStocks';
            _this.shouldRenderChart = _this.methodName != 'AllStocks' && _this.methodName != 'CryptoCurrency' && _this.methodName != 'Currency';
            _this.connectStocks(_this.methodName);
        });
    }
    StocksComponent.prototype.ngOnDestroy = function () {
        console.log("Disconnect:", this.methodName);
        this.stockHubService.disconnectMethod(this.methodName);
        this.onStocksUpdateEmitter.unsubscribe();
        this.routeSubscription.unsubscribe();
    };
    StocksComponent.prototype.connectStocks = function (method) {
        var _this = this;
        this.stockHubService.connectMethod(method, this.onStocksUpdateEmitter);
        this.onStocksUpdateEmitter.subscribe(function (updatedStocks) {
            if (_this.stocks.length == 0) {
                _this.stocks = updatedStocks;
            }
            updatedStocks.forEach(function (newStock) {
                var currentStock = _this.stocks.find(function (y) { return y.title == newStock.title; });
                StockEntity.UpdateStock(currentStock, newStock);
            });
        });
    };
    var _a, _b;
    __decorate([
        Output(),
        __metadata("design:type", typeof (_a = typeof EventEmitter !== "undefined" && EventEmitter) === "function" ? _a : Object)
    ], StocksComponent.prototype, "onStocksUpdateEmitter", void 0);
    StocksComponent = __decorate([
        Component({
            selector: 'stocks',
            templateUrl: './stocks.component.html',
            styleUrls: ['./stocks.component.css']
        }),
        __metadata("design:paramtypes", [StockHubService, typeof (_b = typeof ActivatedRoute !== "undefined" && ActivatedRoute) === "function" ? _b : Object])
    ], StocksComponent);
    return StocksComponent;
}());
export { StocksComponent };
//# sourceMappingURL=stocks.component.js.map