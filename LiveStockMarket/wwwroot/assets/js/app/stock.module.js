var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { SpecificStockComponent } from './components/stocks/specificstock/specificstock.component';
import { StocksComponent } from './components/stocks/stocks.component';
import { TopStocksComponent } from './components/stocks/topstocks/topstocks.component';
import { StockTableComponent } from './shared/components/stocktable/stocktable.component';
import { StockHeaderComponent } from './components/stocks/stockheader/stockheader.component';
import { StockChartComponent } from './shared/components/stockchart/stockchart.component';
import { StockService } from './services/stock.service';
import { StockHubService } from './services/stockhub.service';
import { CurrencyNotationPipe } from './shared/pipes/currencynotationpipe/currencynotation.pipe';
import { DailyDiffPipe } from './shared/pipes/dailydiffpipe/dailydiff.pipe';
export var routes = [
    { path: '', component: StocksComponent },
    { path: 'top', component: TopStocksComponent },
    { path: ':stockName', component: StocksComponent },
    { path: 'detail/:stockName', component: SpecificStockComponent }
];
var StockModule = /** @class */ (function () {
    function StockModule() {
    }
    StockModule = __decorate([
        NgModule({
            declarations: [
                StocksComponent,
                TopStocksComponent,
                StockTableComponent,
                SpecificStockComponent,
                StockHeaderComponent,
                StockChartComponent,
                CurrencyNotationPipe,
                DailyDiffPipe
            ],
            imports: [
                CommonModule,
                HttpModule,
                FormsModule,
                RouterModule.forChild(routes),
            ],
            providers: [
                StockService,
                StockHubService
            ]
        })
    ], StockModule);
    return StockModule;
}());
export { StockModule };
//# sourceMappingURL=stock.module.js.map