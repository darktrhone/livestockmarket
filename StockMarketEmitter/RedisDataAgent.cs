﻿using StackExchange.Redis;
using System.Threading.Tasks;

namespace StockMarketEmitter
{
    public static class RedisDataAgent
    {
        private static IDatabase _database;
        static RedisDataAgent()
        {
            var connection = RedisConnectionFactory.GetConnection();

            _database = connection.GetDatabase();
        }

        public async static Task<string> GetStringValueAsync(string key)
        {
           return await _database.StringGetAsync(key);
        }
    }
}
