﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace StockMarketEmitter
{
    class Program
    {
        public static IConfigurationRoot Config = new ConfigurationBuilder()
                        .SetBasePath(Directory.GetCurrentDirectory())
                        .AddJsonFile("appsettings.json")
                        .Build();

        static HttpClient _httpClient = new HttpClient();

        static string _stocks = string.Empty;


        static string _cryptoCurrency = string.Empty;

        public static void Main(string[] args)
        {
            Configure();

            Start();
        }

        private static void Configure()
        {
            var handler = new HttpClientHandler();
            handler.ClientCertificateOptions = ClientCertificateOption.Automatic;
            handler.SslProtocols = SslProtocols.Tls12;
            handler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true;

            _httpClient = new HttpClient(handler);         
            _httpClient.Timeout = TimeSpan.FromSeconds(10);
            _httpClient.BaseAddress = new Uri(Config.GetValue<string>("AppConfiguration:Endpoints:ServiceCors"));
            _httpClient.DefaultRequestHeaders.Accept.Clear();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        private static void Start()
        {
            while (true)
            {
                GetStocks();

                EmitValues();

                Thread.Sleep(500);
            }
        }

        private async static void GetStocks()
        {
            _stocks = await RedisDataAgent.GetStringValueAsync("Stocks").ConfigureAwait(false);

            _cryptoCurrency = await RedisDataAgent.GetStringValueAsync("CryptoCurrency").ConfigureAwait(false);
        }


        private async static void EmitValues()
        {
            try
            {
                var content = new StringContent(_stocks, Encoding.UTF8, "application/json");

                var cryptoContent = new StringContent(_cryptoCurrency, Encoding.UTF8, "application/json");

                await _httpClient.PostAsync("api/EmitAllStocks", content).ConfigureAwait(false);

                await _httpClient.PostAsync("api/EmitCryptoCurrency", cryptoContent).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}











