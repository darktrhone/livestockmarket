﻿using Microsoft.Extensions.Configuration;
using StackExchange.Redis;
using System;
using System.IO;

namespace StockMarketSyncer
{
    public class RedisConnectionFactory
    {
        private static readonly Lazy<ConnectionMultiplexer> Connection;

        static RedisConnectionFactory()
        {
            try
            {

                var connectionString = Program.Config["Cache:Endpoints:Redis"];

                var options = ConfigurationOptions.Parse(connectionString);

                Connection = new Lazy<ConnectionMultiplexer>(() => ConnectionMultiplexer.Connect(options));
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static ConnectionMultiplexer GetConnection() => Connection.Value;
    }
}