﻿ 
namespace StockMarketSyncer
{
    public class Stock
    {
        public string title;
        public string fullName;
        public double capital;
        public double currentValue;
        public string lastUpdated;
        public double closingValue;
        public string quoteIndex;
        public string dayDiff;
    }
}
