﻿using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace StockMarketSyncer
{

    public static class StockRepository
    {
        private static string ConnectionString => Program.Config["Database:ConnectionStrings:SQLServer2017"];


        static StockRepository()
        {
        }

        public static Task<IDbConnection> OpenConnectionAsync => GetOpenConnectionAsync();


        public async static Task<IEnumerable<Stock>> GetAllStocks()
        {
            using (IDbConnection dbConnection = await OpenConnectionAsync)
            {
                return await dbConnection.QueryAsync<Stock>("SELECT * FROM Stocks;");
            }
        }
        public async static Task<IEnumerable<Stock>> GetAllCryptoCurrencyStocks()
        {
            using (IDbConnection dbConnection = await OpenConnectionAsync)
            {
                return await dbConnection.QueryAsync<Stock>("SELECT * FROM Stocks where QuoteIndex='CryptoCurrency';");
            }
        }

        public async static Task UpdateAllAsync(List<Stock> stockList)
        {
            using (IDbConnection dbConnection = await OpenConnectionAsync)
            {
                var query = QueryBuilder(stockList).ToString();

                await dbConnection.ExecuteAsync(query);
            }
        }

        private async static Task<IDbConnection> GetOpenConnectionAsync()
        {
            var connection = new SqlConnection(ConnectionString);

            await connection.OpenAsync();

            return connection;
        }

        private static StringBuilder QueryBuilder(List<Stock> prods)
        {
            StringBuilder sb = new StringBuilder();

            prods.ForEach(x =>
            {
                sb.AppendFormat("Update Stocks set CurrentValue = {0} where Title = '{1}';", x.currentValue, x.title);
            });

            return sb;
        }
    }
}