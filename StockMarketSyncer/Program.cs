﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace StockMarketSyncer
{
    class Program
    {
        public static IConfigurationRoot Config = new ConfigurationBuilder()
                                .SetBasePath(Directory.GetCurrentDirectory())
                                .AddJsonFile("appsettings.json")
                                .Build();

        private static string _currentDateTime = DateTime.Now.ToString("HH:mm:ss");

        private static List<Stock> _stocks;

        private static List<Stock> _cryptoCurrency;

        private static Random rand = new Random();


        static void Main(string[] args)
        {
            try
            {
                StartStockSyncer();
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        private static void StartStockSyncer()
        {
            PreloadStocks();

            while (true)
            {
                _currentDateTime = DateTime.Now.ToString("HH:mm:ss");

                ProcessStocks();

                SaveStocks();

                Thread.Sleep(500);
            }
        }


        private async static void PreloadStocks()
        {
            try
            {
                _stocks = StockRepository.GetAllStocks().Result.ToList();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private async static void SaveStocks()
        {
            try
            {
                var serializedObj = JsonConvert.SerializeObject(_stocks);

                var serializedCryptoCurrencyObj = JsonConvert.SerializeObject(_stocks.Where(x => x.quoteIndex == "CryptoCurrency"));

                await RedisDataAgent.SetStringValueAsync("Stocks", serializedObj);

                await RedisDataAgent.SetStringValueAsync("CryptoCurrency", serializedCryptoCurrencyObj);

                await StockRepository.UpdateAllAsync(_stocks);
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        private static void ProcessStocks()
        {
            for (int i = 0; i < _stocks.Count; i++)
            {
                var s = _stocks[i];

                double res = (rand.NextDouble() - 0.50);

                var change = (s.currentValue / 1000) * res;

                s.currentValue = Math.Round(s.currentValue + change, 5);

                if (s.currentValue < 0)
                {
                    s.currentValue = 0;
                }
                s.lastUpdated = _currentDateTime;
            }
        }

    }

}