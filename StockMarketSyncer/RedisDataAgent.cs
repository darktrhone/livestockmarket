﻿using StackExchange.Redis;
using System;
using System.Threading.Tasks;

namespace StockMarketSyncer
{
    public static class RedisDataAgent
    {
        private static IDatabase _database;
        static RedisDataAgent()
        {
            try
            {
                var connection = RedisConnectionFactory.GetConnection();

                _database = connection.GetDatabase();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async static Task SetStringValueAsync(string key, string value)
        {
            await _database.StringSetAsync(key, value);
        }
    }
}
